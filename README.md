CLAD
====

Copyright (c) 2018-2025 Marco Antoniotti  
See file COPYING for licensing information


DESCRIPTION
-----------

The Common Lisp Application Directories (CLAD) library is a simple API
collection that provides access to a set of "standard" Common
Lisp folders on a "per-application" or "per-library" basis.

