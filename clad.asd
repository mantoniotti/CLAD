;;;; -*- Mode: Lisp -*-

;;;; clad.asd --

;;;; See file COPYING for licensing information.

(asdf:defsystem #:clad
  :author "Marco Antoniotti"
  :description "The CLAD System."
  :license "BSD"
  :components ((:file "clad-package")
               (:file "clad" :depends-on ("clad-package"))
               )
  )

;;;; end of file -- clad.asd
