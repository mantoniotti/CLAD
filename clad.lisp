;;;; -*- Mode: Lisp -*-

;;;; clad.lisp --

;;;; See file COPYING for licensing information.

(in-package "CLAD")

(defun user-common-lisp-data-directory ()
  "Return the \"user\" Common Lisp Data directory.

Two values are returned, the pathname and whether it exists in the
file system."
  (let ((uadd-pathname
         (merge-pathnames (system-dependent-cl-subfolder-name)
                          (user-homedir-pathname)))
        )
    (declare (type pathname uadd-pathname))

    (values uadd-pathname
            (and (probe-file uadd-pathname) t))
    ))


(defun user-cl-data-folder ()
  "A synonim for USER-COMMON-LISP-DATA-DIRECTORY."
  (user-common-lisp-data-directory))


(defun app-or-library-data-folder (app-or-library-name
                                   &key
                                   version-name
                                   impl-dependent-too
                                   &aux
                                   (addl
                                    (list :relative app-or-library-name))
                                   )
  "Returns the folder (a pathname) for a given app or library."

  (declare (type string app-or-library-name)
           (type (or null string)
                 version-name
                 impl-dependent-too)
           (ignore version-name) ; Too much for the time being.
           (type list addl))
 
  #|
  (when version-name
    (setf addl (nconc addl (list version-name))))
  |#

  (when (string= "" app-or-library-name)
    (warn "CLAD: empty app or library name; top user folder will be returned.")
    (return-from app-or-library-data-folder
      (values (user-common-lisp-data-directory) t)))

  (when impl-dependent-too
    (setf addl (nconc addl (list (lisp-implementation-type)))))

  (let ((a-or-l-dd-pathname
         (merge-pathnames (make-pathname :directory addl)
                          (user-common-lisp-data-directory))))
    (values a-or-l-dd-pathname
            (and (probe-file a-or-l-dd-pathname) t))))


(defun app-or-library-data-folders ()
  "Returns a list of the folder pathnames present in the CL data folder."
  (directory (user-common-lisp-data-directory)))


(defun known-apps-or-libraries-data ()
  "Returns a list of known apps or libraries listed in the CL data folder."
  (mapcar #'(lambda (aldf)
              (declare (type pathname aldf))
              (first (last (pathname-directory aldf))))
          (app-or-library-data-folders)))


(defun ensure-app-or-library-data-folder (app-or-library-name
                                          &key
                                          version-name
                                          impl-dependent-too
                                          )
  "Ensures that the folder for an app or library exists."
  (declare (type string app-or-library-name)
           (type (or null string)
                 version-name
                 impl-dependent-too))
  (let ((addp
         (app-or-library-data-folder app-or-library-name
                                     :version-name version-name
                                     :impl-dependent-too impl-dependent-too))
        )
    (declare (type pathname addp))

    (ensure-directories-exist addp)
    ))


(defun reset-app-or-data-folder (app-or-library-name
				 &optional abort-if-non-empty)
  (declare (type string app-or-library-name)
           (type boolean abort-if-non-empty)
	   (ignorable abort-if-non-empty)
           )
  (when (string/= "" app-or-library-name)
    (let* ((app-lib-data-folder
	    (app-or-library-data-folder app-or-library-name))
	   (content (directory app-lib-data-folder))
	   )
      (if (and content abort-if-non-empty)
	  (abort (make-condition
		  'simple-error
		  :format-control "CLAD: folder '~A' non empty."
		  :format-arguments (list app-lib-data-folder)))
 	  (dolist (f content t)
	    (delete-file f)
	    )))))


;;;; These will go in the 'impl-dependent' folder.

#-sbcl
(defconstant +cl-subfolder-name+
  #+(or macosx darwin)
  "Library/Common Lisp/"

  #+mswindows
  "AppData/Roaming/Common Lisp/"
  
  #+(and (or linux unix) (not (or macosx darwin)))
  ".config/common-lisp/"
  ;; Follows the XDG spec (https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.6.html)
  ;; and ASDF.
  )


;;; Fascist SBCL.

#+sbcl
(defvar +cl-subfolder-name+
  #+(or macosx darwin)
  "Library/Common Lisp/"

  #+mswindows
  "AppData/Roaming/Common Lisp/"
  
  #+(and (or linux unix) (not (or macosx darwin)))
  ".config/common-lisp/"
  ;; Follows the XDG spec (https://specifications.freedesktop.org/basedir-spec/basedir-spec-0.6.html)
  ;; and ASDF.
  )


(defun system-dependent-cl-subfolder-name ()
  "Return the platform dependent Common Lisp pathname.

This function returns a sensible pathname where Common Lisp code and
libraries may reside.

Essentially it follows the XDG specification for Linux and UN*X
systems (but not Mac OS), and 'normal' defaults for MacOS (in
\"~/Library/Common Lisp\") and Windows (in \"AppData/Roaming/Common Lisp\")."
  (pathname +cl-subfolder-name+))


;;;; end of file -- clad.lisp
