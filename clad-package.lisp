;;;; -*- Mode: Lisp -*-

;;;; clad-package.lisp --

;;;; See file COPYING for licensing information.

(defpackage "IT.UNIMIB.DISCO.MA.CLAD" (:use "CL")
  (:nicknames "CLAD")
  (:export
   "USER-COMMON-LISP-DATA-DIRECTORY"
   "USER-CL-DATA-FOLDER"
   "APP-OR-LIBRARY-DATA-FOLDER"
   "APP-OR-LIBRARY-DATA-FOLDERS"
   "KNOWN-APPS-OR-LIBRARIES-DATA"
   "ENSURE-APP-OR-LIBRARY-DATA-FOLDER"
   "RESET-APP-OR-LIBRARY-DATA-FOLDER"
   ))


;;;; end of file -- clad-package.lisp
